# TYL_LidarController

All code is in LidarHesaiXT32Controller.cs - Class for receiving and processing data from a Hesai Lidar XT32
The data is received via UDP and stored in a 3D array of ushort values.
The data is then analysed to find clusters of points and the position of a human is calculated.

Manual of the Lidar: http://www.oxts.com/wp-content/uploads/2021/01/Hesai-PandarXT_User_Manual.pdf

The Lidar should be directly connected to the computer via a network cable.
To receive data on your PC, set the PC's IP address to 192.168.1.100 and subnet mask to 255.255.255.0 (see also manual chapter 2.4)

There is a web interface for the Lidar at : 192.168.1.201/index.html
It can be used to turn on/off the Lidar and to set the rotation speed (20hz or 10hz). This script should handle both, but 20hz is recommended.
