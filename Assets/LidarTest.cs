using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System;

using TMPro;

public class LidarTest : MonoBehaviour
{

    public bool allGood = true;
    public bool listen = true;
    public static int rcvport = 2368;
    int packets = 0;
    int totalPackets = 0;
    float fpsCounter = 1;
    public int channelToDraw = 16;
    public TextMeshPro text;
    public LineRenderer lineRenderer;

    static int lidarDataPreHeaderSize = 6;
    static int lidarDataHeaderSize = 6;
    static int totalHeader = lidarDataHeaderSize + lidarDataPreHeaderSize;

    static int rotationsToStore = 1200; // 60 seconds of 20hz data
    static int channelsToStore = 32;
    static int samplesPerRotation = 1000;  // 1000 at 20hz
    float analysisTime = 0;
    ushort[,] highestDist = new ushort[32,1000];
    int[,] lastTimeHighestDistWasSeen = new int[32,1000];
    ushort[,,] distanceData = new ushort[rotationsToStore, channelsToStore, samplesPerRotation]; // 1200 * 32 * 1000 * 2 = 76800000 bytes = 73MB
    int prevAzimuth = 0;
    int rotations = 0;
    string filestring = "";
    float pointScale = 0.002f;
    List<Vector3> points = new List<Vector3>();
    bool savedDataAlready = false;
    List<int> dataPoints = new List<int>();
    int dataPointCount = 0;
    int currentDistanceIndex = 0;
    int distanceNoise = 2;
    
    void Log(string s, bool render = false)
    {
        Debug.Log(s);
        if (render) text.text = s;
        filestring += s + "\n";
        

    }

    void SaveDataPoints()
    {
        if (savedDataAlready) return;
        savedDataAlready = true;
        Log("Starting save ... " + dataPoints.Count, true);
        filestring = "Saving datapoints, count: " + dataPoints.Count + " time: " + Time.time + "\n";
        File.AppendAllText("datapoints.txt", filestring);
        int index = 0;
        // save datapoints list to file
        for (int i = 0; i < dataPointCount; i++)
        {
            filestring += "id: " + dataPoints[index] + " chunk: " + dataPoints[index + 1] 
                + " ch: " + dataPoints[index + 2] + " az: " + dataPoints[index + 3] + " dist: " + dataPoints[index + 4] + " angleI: " + dataPoints[index + 5] + "\n";
            index += 6;
            if ((i % 100) == 0)
            {
                File.AppendAllText("datapoints.txt", filestring);
                filestring = "";
            }
        }
        dataPoints.Clear();
        Log("Finished save.", true);

    }

    void LoopComplete(int distIndex)
    {
        // draw the points
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
        points.Clear();

        CheckBackgroundStability(distIndex);

    }

    void CheckBackgroundStability(int distIndex)
    {
        int stableCount = 0;
        int maxDist = 5;
        // check distancedata at distindex if they match the background
        for (int ch = 0; ch < 32; ch++)
        {
            for (int az = 0; az < samplesPerRotation; az++)
            {
                int dist = distanceData[distIndex, ch, az];
                int bg = 0;//background[ch, az];
                if (dist > 0 && bg > 0)
                {
                    if (Mathf.Abs(dist - bg) < maxDist)
                    {
                        stableCount++;
                    }
                }
            }
        }
        Log("Stable count: " + stableCount);        

    }


    void AnalyseLidarData(byte[] data)
    {
        float t0 = Time.realtimeSinceStartup;

        if (data.Length == 1080)
        {
            int index = totalHeader;

            for (int i = 0; i < 8; i++)
            {
                int azimuth = data[index] + (data[index + 1] << 8);
                // the angle advances in steps of 0.36 degrees
                int angleIndex = (int)(azimuth / 36);
                
                if (azimuth < prevAzimuth)
                {
                    LoopComplete(currentDistanceIndex);
                    rotations++;
                }
                
                if (angleIndex >= 1000) 
                {
                    Log("Angle index out of range: " + angleIndex);
                    continue;
                }

                index += 2;
                for (int channel = 0; channel < 32; channel++)
                {
                    int dist = data[index] + (data[index + 1] << 8);
                    index += 4;

                    currentDistanceIndex = rotations % rotationsToStore;
                    distanceData[currentDistanceIndex, channel, angleIndex] = (ushort)dist;
                    
                    if (dist > highestDist[channel, angleIndex])
                    {
                        highestDist[channel, angleIndex] = (ushort)dist;
                        lastTimeHighestDistWasSeen[channel, angleIndex] = rotations;
                    }
                    if (highestDist[channel, angleIndex] - dist < distanceNoise)
                    {
                        lastTimeHighestDistWasSeen[channel, angleIndex] = rotations;
                    }

                    if (channel == 16 && dist > 1)
                    {
                        //points.Add(new Vector3(Mathf.Cos(azimuth * 0.01f * Mathf.Deg2Rad) * dist * pointScale, Mathf.Sin(azimuth * 0.01f * Mathf.Deg2Rad) * dist * pointScale, 0));
                        points.Add(new Vector3(angleIndex / 100.0f - 8.0f, dist * pointScale - 4.0f, 0));

                    }
                    //points.Add(new Vector3(dist * Mathf.Cos(angleIndex * 0.36f * Mathf.Deg2Rad), dist * Mathf.Sin(angleIndex * 0.36f * Mathf.Deg2Rad), channel));
                    //Log("Channel " + channel + " angle " + angleIndex + " dist " + dist + " azimuth " + azimuth);
                    /*
                    dataPoints.Add(totalPackets);
                    dataPoints.Add(i);
                    dataPoints.Add(channel);
                    dataPoints.Add(azimuth);
                    dataPoints.Add(dist);
                    dataPoints.Add(angleIndex);
                    dataPointCount++;*/

                }
                
                
                prevAzimuth = azimuth;
                


            }

        }
        else
        {
            Log("Odd data length: " + data.Length);
        }

        float t1 = Time.realtimeSinceStartup - t0;
        analysisTime += t1;
    }


    void StartLidarListenThread()
    {
        listen = true;
        Log("Starting LIDAR Listener Thread");
        Thread listener = new Thread(() =>
        {
            IPEndPoint e = new IPEndPoint(IPAddress.Loopback, 0);
            UdpClient u = new UdpClient(rcvport);

            while (listen)
            {
                try
                {
                    var result = u.Receive(ref e);
                   
                    if (result.Length > 0)
                    {
                        AnalyseLidarData(result);
                        packets += 1;
                        totalPackets += 1;
                    }
                }
                catch (ThreadInterruptedException)
                {
                    Debug.Log("LIDAR Listen Thread interrupted!");
                    allGood = false;
                    break; // exit the while loop
                }
                catch (SocketException)
                {
                    Debug.Log("LIDAR Socket interrupted!");
                    allGood = false;
                    break; // exit the while loop
                }
                catch (Exception ex)
                {
                    Log("ERROR: " + ex);
                }
            }
            Debug.Log("LDIAR Thread stopping.");
        });
        listener.IsBackground = true;
        listener.Start();
    }



    // Start is called before the first frame update
    void Start()
    {
        // write date into log file
        Log("Starting LidarTest at " + System.DateTime.Now.ToString());

        Debug.Log("Starting thread ...");
        StartLidarListenThread();

        // draw a simple triangle using linerenderer
        lineRenderer.positionCount = 3;
        lineRenderer.startWidth = 0.03f;
        lineRenderer.endWidth = 0.03f;
        lineRenderer.SetPositions(new Vector3[] { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0) });

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > fpsCounter)
        {
            string s = "";
            if (packets > 0)
            {
                s = "Runtime: "+Time.time+" Packets sent this frame: " + packets + " (total: " + totalPackets+ ") average time between packets: " + (1000.0f / packets) 
                + " ms \nTotal Analysis time: " + (analysisTime * 1000.0f) + "ms, per packet: " + (analysisTime * 1000.0f / packets) + "ms \nRotations: " + rotations;
            }
            else
            {
                s = "Runtime: "+Time.time+" Packets sent this frame: " + packets + " (total: " + totalPackets+ ") ";
            }
            Log(s, true);
            analysisTime = 0;
            packets = 0;
            fpsCounter += 1;
            rotations = 0;
            File.AppendAllText("log.txt", filestring);
            filestring = "";
            //if (Time.time > 2) SaveDataPoints();
        }
    }

    void OnApplicationQuit()
    {
        Log("Application ending after " + Time.time + " seconds");
        listen = false;
    }
}
