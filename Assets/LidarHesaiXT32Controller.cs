using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System;
using TMPro;

/*
LidarHesaiXT32Controller.cs - Class for receiving and processing data from a Hesai Lidar XT32
The data is received via UDP and stored in a 3D array of ushort values.
The data is then analysed to find clusters of points and the position of a human is calculated.

Manual of the Lidar: http://www.oxts.com/wp-content/uploads/2021/01/Hesai-PandarXT_User_Manual.pdf

The Lidar should be directly connected to the computer via a network cable.
To receive data on your PC, set the PC's IP address to 192.168.1.100 and subnet mask to 255.255.255.0 (see also manual chapter 2.4)

There is a web interface for the Lidar at : 192.168.1.201/index.html
It can be used to turn on/off the Lidar and to set the rotation speed (20hz or 10hz). This script should handle both, but 20hz is recommended.

*/

public class LidarHesaiXT32Controller : MonoBehaviour
{
    public bool superSimpleAnalysis = true; // use the simplest algorithm for now

    public bool showDebugText = false;
    public bool drawLidarData = false;
    public bool allowDebugKeyboardInput = true;

    
    public float unfilteredPlayerX = 0;
    public float unfilteredPlayerY = 0;
    
    public float filterMinCutoff = 1f;
    public float filterBeta = 0f;
    public float filterDerivateCutoff = 1f;
    
    public float playerX = 0;
    public float playerY = 0;
    

    public bool playerFound = false;
    public bool playerOutOfBounds = true;

    // Network variables
    public static int rcvport = 2368;
    bool listen = true;
    int packets = 0;
    int totalPackets = 0;
    static int lidarDataPreHeaderSize = 6;
    static int lidarDataHeaderSize = 6;
    static int totalHeader = lidarDataHeaderSize + lidarDataPreHeaderSize;

    static int rotationsToStore = 1200; // 60 seconds of 20hz data
    static int channelsToStore = 32;
    static int samplesPerRotation = 1000;  // 1000 at 20hz

    // playing square variables
    public float playingAreaCenterAngle = 0f; // angle as offset from angle 0 of LIDAR
    public float playingAreaWidth = 5f; // meters
    public float playingAreaLength = 6f; // meters
    public float playingAreaDistanceFromLidar = 2f; // meters
    public float playingAreaMarginArea = 0.5f; // meters, area around playing area that tracked, but not inside playing area proper

    public LineRenderer playingAreaLineRenderer;


    // Drawing variables
    public TextMeshPro text;
    public LineRenderer lineRenderer;
    public LineRenderer backgroundLineRenderer;
    public LineRenderer clusterLineRenderer;
    public Transform playerObject;
    public Transform rawPlayerObject;

    // Analysis variables
    float analysisTime = 0;
    ushort[,] highestDist = new ushort[32,1000];
    int[,] lastTimeHighestDistWasSeen = new int[32,1000];
    int[,] backgroundStrength = new int[32, 1000];
    int[,] backgroundDistance = new int[32, 1000];
    int[,] clusterNumber = new int[32, 1000];
    List<int> clusters = new List<int>();
    ushort[,,] distanceData = new ushort[rotationsToStore, channelsToStore, samplesPerRotation]; // 1200 * 32 * 1000 * 2 = 76800000 bytes = 73MB
    ushort[] currentRotationData = new ushort[samplesPerRotation]; 
    ushort[] previousRotationData = new ushort[samplesPerRotation]; 
    int distanceNoise = 5; // 5 * 4mm = 20mm
    int maxClusterJumpDistance = 50; // 50 * 4mm = 200mm
    int runningClusterNumber = 1;
    int clustersLayer16 = 0;

    int previousClusterEndAngle = -1;
    int previousClusterEndDist = 0;


    List<Cluster> clustersToDraw = new List<Cluster>();

    Cluster biggestCluster;

    List<Color> debugColors = new List<Color>();
    float fpsCounter = 1;

    int prevAzimuth = 0;
    int prevAngleIndex = -1;
    int dropCount = 0;
    int rotations = 0;
    string filestring = "";
    float pointScale = 0.004f;
    List<Vector3> points = new List<Vector3>();
    List<Vector3> backgroundPoints = new List<Vector3>();
    List<Vector3> clusterPoints = new List<Vector3>();
    bool saveRawData = false;
    int rawDataSaved = 0;
    List<int> dataPoints = new List<int>();
    int currentDistanceIndex = 0;
    FileStream fileStream;
    bool startStreamSoon = false;
    bool stopStreamSoon = false;

    bool startPlaybackSoon = false;
    bool playingFromFile = false;
    int filePosition = 0;
    long nextPackageTime = 0;

    Vector3[] pointArray = new Vector3[1];

    EventWaitHandle wh = new AutoResetEvent (false);
    Thread analysisThread;
    readonly object locker = new object();
    Queue<int[]> pointsToAnalyse = new Queue<int[]>();
    int maxQueueSize = 0;

    Stopwatch analysisStopwatch = new Stopwatch();

    OneEuroFilter<Vector2> playerPositionFilter = new OneEuroFilter<Vector2>(20.0f);

    class Cluster
    {
        public int id;
        public int size;
        public int channel;
        public int angleIndex;
        public int sumDistance;
        public int medianDistance;
        public float averageDistance;
        public int angularSize; 
        public float avgAngle;

        public float sizeInMeters;

        public bool isHuman = false;
        public int humanId = -1;
    }


    void Log(string s, bool render = false)
    {
        UnityEngine.Debug.Log(s);
        if (render) text.text = s;
        filestring += s + "\n";
    }

    void SaveRawData(byte[] data, FileStream stream)
    {
        stream.Write(data, 0, data.Length);
        rawDataSaved += data.Length;
    }

    void StartPlayback()
    {
        Log("Starting playback");
        string[] files = Directory.GetFiles(".", "*.lidarlog");
        if (files.Length > 0)
        {
            string file = files[0];
            Log("Loading file " + file);
            fileStream = new FileStream(file, FileMode.Open);
            playingFromFile = true;
            filePosition = 0;
            nextPackageTime = DateTime.Now.Ticks;
        }
    }

    void LookAtNextPlaybackData()
    {
        byte[] data = new byte[1080];
        int read = fileStream.Read(data, 0, 1080);
        //Log("Looking at next playback data, read " + read + " bytes, file pos: " + filePosition);
        if (read == 1080)
        {
            AnalyseLidarData(data);
            filePosition += 1080;
            nextPackageTime += 4000;
            
            packets += 1;
            totalPackets += 1;
        }
        else
        {
            Log("End of file");
            playingFromFile = false;
            fileStream.Close();
        }
    }

    public bool IsInPlayingArea(int angle, int dist)
    {
        float angleInRadians = (angle * 0.36f  - playingAreaCenterAngle) * Mathf.Deg2Rad;
        float x = Mathf.Cos(angleInRadians) * dist * pointScale;
        float y = Mathf.Sin(angleInRadians) * dist * pointScale;
        float playingAreaCenterX = 0;
        float playingAreaCenterY = playingAreaDistanceFromLidar + playingAreaLength / 2;
        float playingAreaLeftX = playingAreaCenterX - playingAreaWidth / 2;
        float playingAreaRightX = playingAreaCenterX + playingAreaWidth / 2;
        float playingAreaTopY = playingAreaCenterY + playingAreaLength / 2;
        float playingAreaBottomY = playingAreaCenterY - playingAreaLength / 2;
        return (x > playingAreaLeftX && x < playingAreaRightX && y > playingAreaBottomY && y < playingAreaTopY);
    }

    public List<Vector3> GetPlayingAreaVectors()
    {
        // return vectors for Line Renderer that describe the rectangle given by the playing area variables
        // note that we never rotate this rectangle, but instead rotate the lidar data points by playingAreaCenterAngle
        float playingAreaCenterX = 0; 
        float playingAreaCenterY = playingAreaDistanceFromLidar + playingAreaLength / 2;

        float playingAreaLeftX = playingAreaCenterX - playingAreaWidth / 2;
        float playingAreaRightX = playingAreaCenterX + playingAreaWidth / 2;
        float playingAreaTopY = playingAreaCenterY + playingAreaLength / 2;
        float playingAreaBottomY = playingAreaCenterY - playingAreaLength / 2;

        List<Vector3> vectors = new List<Vector3>();
        vectors.Add(new Vector3(playingAreaLeftX, playingAreaTopY, 0));
        vectors.Add(new Vector3(playingAreaRightX, playingAreaTopY, 0));
        vectors.Add(new Vector3(playingAreaRightX, playingAreaBottomY, 0));
        vectors.Add(new Vector3(playingAreaLeftX, playingAreaBottomY, 0));

        return vectors;
    }

    void DrawPlayingArea()
    {
        List<Vector3> vectors = GetPlayingAreaVectors();
        playingAreaLineRenderer.positionCount = vectors.Count;
        playingAreaLineRenderer.startWidth = 0.03f;
        playingAreaLineRenderer.endWidth = 0.03f;
        playingAreaLineRenderer.SetPositions(vectors.ToArray());
    }

    void LoopComplete(int distIndex, int currentRotations)
    {
        // for drawing the lidar data in the main thread, make a copy of the points data
        pointArray = points.ToArray();
        points.Clear();

        int currentRotationIndex = currentRotations % rotationsToStore;
        AnalyseClusters(currentRotationIndex);

        TrackPlayer();


        Dictionary <int, int> clusterSizes = new Dictionary<int, int>();
        // count the number of different clusters in clusterNumber array at layer 16
        for (int i = 0; i < 1000; i++)
        {
            if (clusterNumber[16, i] > 0)
            {
                if (clusterSizes.ContainsKey(clusterNumber[16, i]))
                {
                    clusterSizes[clusterNumber[16, i]] += 1;
                }
                else
                {
                    clusterSizes[clusterNumber[16, i]] = 1;
                }
            }
        }

        int largestCluster = 0;
        foreach (KeyValuePair<int, int> entry in clusterSizes)
        {
            if (entry.Value > largestCluster) largestCluster = entry.Value;
        }
        clustersLayer16 = largestCluster;


        clusterNumber = new int[32, 1000];
        previousClusterEndAngle = -1;
        previousClusterEndDist = -1;
        runningClusterNumber = 1;

        clusters.Clear();
        // save data
        if (startStreamSoon)
        {
            if (fileStream != null) fileStream.Close();
            fileStream = new FileStream("rawdata" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".lidarlog", FileMode.Create);
            saveRawData = true;
            Log("Starting stream");
            startStreamSoon = false;
        }

        if (stopStreamSoon)
        {
            saveRawData = false;
            fileStream.Close();
            stopStreamSoon = false;
            Log("Stopping stream");
        }

    }

    void DrawLine(int angleIndex1, int dist1, int angleIndex2, int dist2, int colorindex)
    {
        Vector3 p1 = new Vector3(dist1 * Mathf.Cos(angleIndex1 * 0.36f * Mathf.Deg2Rad), dist1 * Mathf.Sin(angleIndex1 * 0.36f * Mathf.Deg2Rad), 0);
        Vector3 p2 = new Vector3(dist2 * Mathf.Cos(angleIndex2 * 0.36f * Mathf.Deg2Rad), dist2 * Mathf.Sin(angleIndex2 * 0.36f * Mathf.Deg2Rad), 0);
        UnityEngine.Debug.DrawLine(p1, p2, debugColors[colorindex % debugColors.Count], 0.1f);
    }

    float GetDistanceBetweenTwoPoints(int angleIndex1, int dist1, int angleIndex2, int dist2)
    {
        Vector3 p1 = new Vector3(dist1 * Mathf.Cos(angleIndex1 * 0.36f * Mathf.Deg2Rad) * pointScale, dist1 * Mathf.Sin(angleIndex1 * 0.36f * Mathf.Deg2Rad) * pointScale, 0);
        Vector3 p2 = new Vector3(dist2 * Mathf.Cos(angleIndex2 * 0.36f * Mathf.Deg2Rad) * pointScale, dist2 * Mathf.Sin(angleIndex2 * 0.36f * Mathf.Deg2Rad) * pointScale, 0);
        return Vector3.Distance(p1, p2);
    }

    void MergeCloseClusters()
    {
        // merge clusters that are closer together than 0.5m


    }

    // analyse data:
    // check if it's background: if yes, add to backgroundStrength
    //      if no: 
    //          is it further away? weaken background
    //          is it closer? it's an obstacle candidate

    // for all obstacle candidates:
    //      calculate continuous size of obstacle, build clusters

    // for all clusters: 
    //      evaluate if it's a human - check if there's corresponding clusters in different layers

    // for all humans: 
    //      check if there's been a human there in a previous rotation, if yes: assign id, if no: create new human (probably ignore)

    // for all rejected clusters: weaken background if the distance is stable

    void AnalyseClusters(int rotationIndex)
    {
        // every full turn, let's check the clusters
        //Log("Analysing Clusters. Clusters found this rotation: " + clusters.Count);
        int filteredClusters = 0;
        float biggestClusterSize = 0;

        foreach (var c in clusters.ToArray())
        {
            int channel = c / 1000;
            int angleIndex = c % 1000;
            int id = clusterNumber[channel, angleIndex];
            int currentAngle = angleIndex;

            if (angleIndex == 0)
            {
                // fuse with cluster at the end of the rotation
                for (int i = 999; i > angleIndex; i--)
                {
                    if (clusterNumber[channel, i] > 0)
                    {
                        clusterNumber[channel, i] = id;
                        currentAngle = i;
                    }
                    else
                    {
                        break;
                    }
                }
            }


            int angularSize = 0;            
            var distList = new List<int>();
            int minDist = 10000000;
            int maxDist = 0;
            int sumDistance = 0;
            int entriesWithDistance = 0;
            while(clusterNumber[channel, currentAngle] == id)
            {
                var dist =  previousRotationData[currentAngle]; // distanceData[rotationIndex, channel, currentAngle];
                sumDistance += dist;
                entriesWithDistance++;
                distList.Add(dist);
                currentAngle++;

                if (dist < minDist) minDist = dist;
                if (dist > maxDist) maxDist = dist;
                if (currentAngle >= 1000) 
                {
                    currentAngle = 0;
                    Log("Cluster is crossing 0!");
                }
                if (currentAngle == angleIndex) 
                {
                    // we've come full circle
                    Log("Cluster is full circle!");
                    break;
                }
                if (clusterNumber[channel, currentAngle] == 0)
                {
                    // check if the next cluster has the same id
                    for (int i = currentAngle; i < 1000; i++)
                    {
                        if (clusterNumber[channel, i] == id)
                        {
                            currentAngle = i;
                        }
                        if (clusterNumber[channel, i] > 0)
                        {
                            // we've found a different cluster
                            break;
                        }
                    }
                }

            }
            angularSize = currentAngle - angleIndex;
            if (angularSize < 0) angularSize += 1000; // in case we've crossed 0

            // start filtering small clusters
            if (angularSize < 5 || (maxDist - minDist) < 1 || angularSize == 1000) 
            {
                filteredClusters++;
                //continue;
            }
            var cluster = new Cluster();
            cluster.id = id;
            cluster.channel = channel;
            cluster.angleIndex = angleIndex;
            cluster.angularSize = angularSize;
            cluster.sumDistance = sumDistance;
            cluster.averageDistance = (float)cluster.sumDistance / (float)entriesWithDistance;
            cluster.medianDistance = distList[distList.Count / 2];
            cluster.avgAngle = (float)angleIndex + (float)cluster.angularSize / 2f;
            if (cluster.avgAngle > 1000) cluster.avgAngle -= 1000;
            cluster.avgAngle = (float)cluster.avgAngle / 1000f * 360f;
            
            cluster.sizeInMeters = cluster.angularSize * 0.36f * Mathf.Deg2Rad * cluster.averageDistance * pointScale;

            if (cluster.averageDistance > 4000)
            {
                Log("Cluster is too far away: " + cluster.averageDistance 
                + " sumDistance: " + cluster.sumDistance + " angularSize: " + cluster.angularSize + " angleIndex: " 
                + cluster.angleIndex + " channel: " + cluster.channel + " id: " + cluster.id);

                // print distList
                string distListString = "";
                foreach (var d in distList)
                {
                    distListString += d + ", ";
                }
                Log("DistList: " + distListString);

                
            }

            if (cluster.sizeInMeters > biggestClusterSize)
            {
                biggestClusterSize = cluster.sizeInMeters;
                biggestCluster = cluster;
            } 

            if (channel == 16 && drawLidarData)
            {
                clustersToDraw.Add(cluster);
            }
        }

        

    }

    void SmoothPlayerPosUpdate()
    {
        // called by Update rather than the Lidar Analysis, giving us a smoother player position update
        if (playerFound)
        {
                var playerVector = new Vector2(unfilteredPlayerX, unfilteredPlayerY);

                playerVector = playerPositionFilter.Filter(playerVector, Time.time);

                playerX = playerVector.x;
                playerY = playerVector.y; 

        }
    }

    void TrackPlayer()
    {
        if (biggestCluster == null) 
        {
            playerFound = false;

            return;

        }
        float x = Mathf.Cos((biggestCluster.avgAngle - playingAreaCenterAngle) * Mathf.Deg2Rad) * biggestCluster.medianDistance * pointScale;
        float y = Mathf.Sin((biggestCluster.avgAngle - playingAreaCenterAngle) * Mathf.Deg2Rad) * biggestCluster.medianDistance * pointScale;

        Log("Player at " + x + ", " + y + " (" + biggestCluster.avgAngle + "°, " + biggestCluster.medianDistance + "mm, " + biggestCluster.sizeInMeters + "m)");
        if (biggestCluster.sizeInMeters > 0.05f)
        {    
            playerFound = true;
            bool inside =  IsInPlayingArea((int) (biggestCluster.avgAngle / 0.36f), (int) biggestCluster.medianDistance);
            playerOutOfBounds = !inside;
            var playerVector = new Vector2(x, y);
            bool bigMovement = false;
            if (Mathf.Abs(playerVector.x - unfilteredPlayerX) > 0.5f || Mathf.Abs(playerVector.y - unfilteredPlayerY) > 0.5f)
            {
                bigMovement = true;
            }
        
            unfilteredPlayerX = x;
            unfilteredPlayerY = y;

            if (!bigMovement)
            {
                //playerVector = playerPositionFilter.Filter(playerVector);

                //playerX = playerVector.x;
                //playerY = playerVector.y;
            }

        }
        else
        {
            playerFound = false;
        }
    }

    void EnqueuePoint(int[] p)
    {
        lock(locker) 
        {
            pointsToAnalyse.Enqueue(p);
            maxQueueSize = Math.Max(maxQueueSize, pointsToAnalyse.Count);
        }
        wh.Set();
    }

    void AnalyseLidarPointSimple(int dist, int angleIndex, int channel, int prevDist)
    {
        // very simple analysis. Just look at channel 16
        if (channel != 16) return;

        // check if it's within the play area
        if (!IsInPlayingArea((int) angleIndex, dist)) return;

        int currentCluster = 0;
        // check if previous point is also in the playing area
        if (angleIndex > 0)
        {

            

            if (clusterNumber[channel, angleIndex - 1] == 0) 
            {
                bool mergeClusters = false;

                // check if previous cluster end is really close to this point
                if (previousClusterEndAngle != -1)
                {
                    float dist2 = GetDistanceBetweenTwoPoints(angleIndex, dist, previousClusterEndAngle, previousClusterEndDist);
                    if (dist2 < 0.5f)
                    {
                        Log("Previous cluster end is close to this point: " + dist2 + "m, joining clusters");
                        currentCluster = clusterNumber[channel, previousClusterEndAngle];
                        mergeClusters = true;
                    }

                }

                if (!mergeClusters)
                {
                    // new cluster!
                    runningClusterNumber++;
                    clusters.Add(channel * 1000 + angleIndex);
                    currentCluster = runningClusterNumber;
                }
            }
            else
            {
                currentCluster = clusterNumber[channel, angleIndex - 1];
            }
        }
        else
        {
            currentCluster = runningClusterNumber;
        }
        clusterNumber[channel, angleIndex] = currentCluster;
        previousClusterEndAngle = angleIndex;
        previousClusterEndDist = dist;
    }

    void AnalyseLidarPoint(int dist, int angleIndex, int channel, int prevDist)
    {
        // analyse the point and see if it's part of the background. work in progress!

        // check if it's the furthest point we've seen
        if (dist > highestDist[channel, angleIndex])
        {
            highestDist[channel, angleIndex] = (ushort)dist;
            lastTimeHighestDistWasSeen[channel, angleIndex] = rotations;
        }
        if (highestDist[channel, angleIndex] - dist < distanceNoise)
        {
            lastTimeHighestDistWasSeen[channel, angleIndex] = rotations;
        }

        // calculate backgroundStrength
        int diff = Math.Abs(backgroundDistance[channel, angleIndex] - dist);
        int currentCluster = 0;

        if (diff < distanceNoise)
        {
            // part of background!
            backgroundStrength[channel, angleIndex]++;
            if (backgroundStrength[channel, angleIndex] > 100) backgroundStrength[channel, angleIndex] = 100;
        }
        else
        {
            // distance is further away: weaken background
            if (dist > backgroundDistance[channel, angleIndex])
            {
                backgroundStrength[channel, angleIndex]--;
                if (backgroundStrength[channel, angleIndex] <= 0)
                {
                    backgroundDistance[channel, angleIndex] = dist;
                    backgroundStrength[channel, angleIndex] = 5;
                }
            }
            else if (dist < backgroundDistance[channel, angleIndex] - distanceNoise * 5)
            {
                // part of a cluster!
                // increase cluster if previous distance on same channel wasn't part of a cluster (i.e., new cluster start!)
                if (angleIndex > 0)
                {
                    if (clusterNumber[channel, angleIndex - 1] == 0) 
                    {
                        // new cluster!
                        runningClusterNumber++;
                        clusters.Add(channel * 1000 + angleIndex);
                        currentCluster = runningClusterNumber;
                    }
                    else
                    {
                        // check distance of previous point
                        if (Mathf.Abs(prevDist - dist) < maxClusterJumpDistance)
                        {
                            // part of same cluster!
                            currentCluster = clusterNumber[channel, angleIndex - 1];
                        }
                        else
                        {
                            // new cluster!
                            runningClusterNumber++;
                            clusters.Add(channel * 1000 + angleIndex);
                            currentCluster = runningClusterNumber;
                        }
                        currentCluster = clusterNumber[channel, angleIndex - 1];
                    }
                }
                else
                {
                    currentCluster = runningClusterNumber;
                }
            }
        }

        // if current distance is closer than background, currentCluster is > 0, see above
        clusterNumber[channel, angleIndex] = currentCluster;

        if (angleIndex > 0 && currentCluster == 0 && clusterNumber[channel, angleIndex - 1] != 0)
        {
            // cluster stopped!
        }

    }


    void AnalyseLidarData(byte[] data)
    {
        //float t0 = Time.realtimeSinceStartup;

        if (data.Length == 1080)
        {
            int index = totalHeader;
            if (saveRawData) SaveRawData(data, fileStream);
            for (int i = 0; i < 8; i++)
            {
                int azimuth = data[index] + (data[index + 1] << 8);
                // the angle advances in steps of 0.36 degrees
                int angleIndex = (int)(azimuth / 36);
                


                if (azimuth < prevAzimuth)
                {

                    // copy current rotation data to previous rotation data
                    for (int j = 0; j < 1000; j++)
                    {
                        previousRotationData[j] = currentRotationData[j];
                    }
                    LoopComplete(currentDistanceIndex, rotations);


                    rotations++;
                }

                if (prevAngleIndex != angleIndex - 1)
                {
                    if (prevAngleIndex != -1 && !(prevAngleIndex == 999 && angleIndex == 0))
                        dropCount++;
                }
                prevAngleIndex = angleIndex;

                if (angleIndex >= 1000) 
                {
                    Log("Angle index out of range: " + angleIndex);
                    continue;
                }

                index += 2;
                for (int channel = 0; channel < 32; channel++)
                {
                    int dist = data[index] + (data[index + 1] << 8);
                    index += 4;

                    currentDistanceIndex = rotations % rotationsToStore;
                    distanceData[currentDistanceIndex, channel, angleIndex] = (ushort)dist;
                    if (channel == 16) currentRotationData[angleIndex] = (ushort)dist;
                    int prevDist = 0;
                    if (angleIndex > 0) prevDist = distanceData[currentDistanceIndex, channel, angleIndex - 1];
                    
                    var point = new int[]{dist, angleIndex, channel, prevDist};
                    EnqueuePoint(point);
                    //AnalyseLidarPointSimple(dist, angleIndex, channel, prevDist);

                    if (channel == 16 && dist > 1 && drawLidarData)
                    {
                        points.Add(new Vector3(Mathf.Cos((angleIndex * 0.36f  - playingAreaCenterAngle) * Mathf.Deg2Rad) * dist * pointScale, 
                            Mathf.Sin((angleIndex * 0.36f  - playingAreaCenterAngle) * Mathf.Deg2Rad) * dist * pointScale, 0));
                    }
                }
                
                
                prevAzimuth = azimuth;
                


            }

        }
        else
        {
            Log("Odd data length: " + data.Length);
        }

        //float t1 = Time.realtimeSinceStartup - t0;
        analysisTime += 0;
    }

    void StartAnalysisThread()
    {
        Log("Starting Analysis Thread");
        analysisThread = new Thread(() =>
        {
            while (true)
            {
                int[] data = null;
                lock(locker)
                {
                    if (pointsToAnalyse.Count > 0)
                    {
                        data = pointsToAnalyse.Dequeue();
                        if (data == null) return;
                    }
                }
                if (data != null)
                {
                    analysisStopwatch.Start();
                    if (superSimpleAnalysis)
                        AnalyseLidarPointSimple(data[0], data[1], data[2], data[3]);
                    else
                        AnalyseLidarPoint(data[0], data[1], data[2], data[3]);
                    analysisStopwatch.Stop();
                    analysisTime = analysisStopwatch.ElapsedTicks / (float) Stopwatch.Frequency;
                }
                else
                {
                    wh.WaitOne();
                }
            }
        });
        analysisThread.IsBackground = true;
        analysisThread.Start();
    }

    void StartLidarListenThread()
    {
        listen = true;
        Log("Starting LIDAR Listener Thread");
        Thread listener = new Thread(() =>
        {
            IPEndPoint e = new IPEndPoint(IPAddress.Loopback, 0);
            UdpClient u = new UdpClient(rcvport);

            while (listen)
            {
                try
                {

                    if (startPlaybackSoon)
                    {
                        startPlaybackSoon = false;
                        StartPlayback();
                    }

                    if (playingFromFile && DateTime.Now.Ticks > nextPackageTime)
                    {
                        LookAtNextPlaybackData();
                    }

                    if (u.Available > 0)
                    {
                        var result = u.Receive(ref e);

                        AnalyseLidarData(result);
                        packets += 1;
                        totalPackets += 1;
                    }
                }
                catch (ThreadInterruptedException)
                {
                    Log("LIDAR Listen Thread interrupted!");
                    break; // exit the while loop
                }
                catch (SocketException)
                {
                    Log("LIDAR Socket interrupted!");
                    break; // exit the while loop
                }
                catch (Exception ex)
                {
                    Log("ERROR: " + ex);
                }
            }
            Log("LDIAR Thread stopping.");
        });
        listener.IsBackground = true;
        listener.Start();
    }

    // Start is called before the first frame update
    void Start()
    {
        // write date into log file
        Log("Starting LidarTest at " + System.DateTime.Now.ToString());

        Log("Starting threads ...");
        StartLidarListenThread();
        StartAnalysisThread();
        
        // draw a simple triangle using linerenderer
        lineRenderer.positionCount = 3;
        lineRenderer.startWidth = 0.03f;
        lineRenderer.endWidth = 0.03f;
        lineRenderer.SetPositions(new Vector3[] { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0) });

        backgroundLineRenderer.positionCount = 3;
        backgroundLineRenderer.startWidth = 0.03f;
        backgroundLineRenderer.endWidth = 0.03f;
        backgroundLineRenderer.SetPositions(new Vector3[] { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0) });

        debugColors.Add(Color.blue);
        debugColors.Add(Color.green);
        debugColors.Add(Color.yellow);
        debugColors.Add(Color.gray);
    }

    void DrawBackground()
    {
        int channel = 16;
        backgroundPoints.Clear();
        for (int i = 0; i < 1000; i++)
        {
            int dist = backgroundDistance[channel, i];
            if (dist > 1)
            {
                backgroundPoints.Add(new Vector3(Mathf.Cos(i * 0.36f * Mathf.Deg2Rad) * dist * pointScale, Mathf.Sin(i * 0.36f * Mathf.Deg2Rad) * dist * pointScale, 0));
            }
        }
        backgroundLineRenderer.positionCount = backgroundPoints.Count;
        backgroundLineRenderer.SetPositions(backgroundPoints.ToArray());

    }

    void DrawClusters()
    {
        // find biggest cluster and draw that one
        if (clustersToDraw.Count == 0) return;
        Cluster biggestCluster = clustersToDraw[0];
        int biggestClusterSize = 0;
        string allClusters = "[DrawClusters] count: " + clustersToDraw.Count + " angular sizes: ";
        for (int i = 0; i < clustersToDraw.Count; i++)
        {
            if (i >= clustersToDraw.Count) break;
            Cluster c = clustersToDraw[i];
            if (c.angularSize > biggestClusterSize)
            {
                biggestClusterSize = c.angularSize;
                biggestCluster = c;
            }
            allClusters += c.angularSize + " ";
        }
        if (biggestClusterSize == 0) return;

        Log("[DrawClusters] Biggest clusters: " + biggestCluster.averageDistance + " " + biggestCluster.avgAngle + " " + biggestCluster.angularSize);
        Log(allClusters);
        
        var p = new Vector3(Mathf.Cos(biggestCluster.avgAngle * Mathf.Deg2Rad) * biggestCluster.averageDistance * pointScale, Mathf.Sin(biggestCluster.avgAngle  * Mathf.Deg2Rad) * biggestCluster.averageDistance * pointScale, 0);

        clusterLineRenderer.positionCount = 3;
        clusterLineRenderer.startWidth = 0.03f;
        clusterLineRenderer.endWidth = 0.03f;
        clusterLineRenderer.SetPositions(new Vector3[] { p + new Vector3(0, 0, 0), p + new Vector3(1, 0, 0), p + new Vector3(0, 1, 0) });        

        clustersToDraw.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > fpsCounter)
        {
            if (showDebugText)
            {
                string s = "";
                if (packets > 0)
                {
                    s = "Runtime: "+Time.time+" Packets sent this frame: " + packets + " (total: " + totalPackets+ ") average time between packets: " + (1000.0f / packets) 
                    + " ms \nTotal Analysis time: " + (analysisTime * 1000.0f) + "ms, per packet: " + (analysisTime * 1000.0f / packets) + "ms \nRotations: " + rotations
                    + "\nDropped Packages: " + dropCount + " recording: " + saveRawData + " maxQueueSize: " + maxQueueSize + " queueSize: " + pointsToAnalyse.Count
                    + "\nCluster count: " + clustersLayer16;
                    if (saveRawData)
                    {
                        s += "\nData saved: " + rawDataSaved + " bytes, " + rawDataSaved / 1080 + " packages";
                    }
                }
                else
                {
                    s = "Runtime: "+Time.time+" Packets sent this frame: " + packets + " (total: " + totalPackets+ ") ";
                }
                Log(s, true);
            }

            
            analysisTime = 0;
            analysisStopwatch.Reset();
            packets = 0;
            fpsCounter += 1;
            rotations = 0;
            //File.AppendAllText("log.txt", filestring);
            filestring = "";
            //DrawBackground();
            if (drawLidarData) DrawPlayingArea();

            // update filter settings
            playerPositionFilter.UpdateParams(20.0f, filterMinCutoff, filterBeta, filterDerivateCutoff);

        }
        SmoothPlayerPosUpdate();
        if (drawLidarData) 
        {
            DrawClusters();
            if (playerObject != null)
            {
                if (playerFound && !playerOutOfBounds)
                {
                    playerObject.position = new Vector3(playerX, playerY, 0);
                    if (rawPlayerObject != null) rawPlayerObject.position = new Vector3(unfilteredPlayerX, unfilteredPlayerY, 0.14f);
                    playerObject.GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    playerObject.GetComponent<Renderer>().material.color = Color.red;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && allowDebugKeyboardInput)
        {
            if (saveRawData)
            {
                Log("Space pressed - stopping stream with next full rotation");
                stopStreamSoon = true;
            }
            else
            {
                Log("Space pressed - starting stream with next full rotation");
                startStreamSoon = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.P) && allowDebugKeyboardInput)
        {
            //rawdata20230103114205658
            Log("P pressed, playing back data from file!");
            startPlaybackSoon = true;
        }

        // draw background when B is pressed
        if (Input.GetKeyDown(KeyCode.B) && allowDebugKeyboardInput)
        {
            DrawBackground();
        }

        if (drawLidarData && pointArray != null && pointArray.Length > 0)
        {
            lineRenderer.positionCount = pointArray.Length;
            lineRenderer.SetPositions(pointArray);
            pointArray = new Vector3[0];
        }
    }

    void OnApplicationQuit()
    {
        Log("Application ending after " + Time.time + " seconds");
        listen = false;
    }
}
